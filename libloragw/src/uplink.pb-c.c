/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: uplink.proto */

/* Do not generate deprecated warnings for self */
#ifndef PROTOBUF_C__NO_DEPRECATED
#define PROTOBUF_C__NO_DEPRECATED
#endif

#include "uplink.pb-c.h"
void   with_metadata_from_device__init
                     (WithMetadataFromDevice         *message)
{
  static const WithMetadataFromDevice init_value = WITH_METADATA_FROM_DEVICE__INIT;
  *message = init_value;
}
size_t with_metadata_from_device__get_packed_size
                     (const WithMetadataFromDevice *message)
{
  assert(message->base.descriptor == &with_metadata_from_device__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t with_metadata_from_device__pack
                     (const WithMetadataFromDevice *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &with_metadata_from_device__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t with_metadata_from_device__pack_to_buffer
                     (const WithMetadataFromDevice *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &with_metadata_from_device__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
WithMetadataFromDevice *
       with_metadata_from_device__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (WithMetadataFromDevice *)
     protobuf_c_message_unpack (&with_metadata_from_device__descriptor,
                                allocator, len, data);
}
void   with_metadata_from_device__free_unpacked
                     (WithMetadataFromDevice *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &with_metadata_from_device__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   with_metadata_to_gateway__init
                     (WithMetadataToGateway         *message)
{
  static const WithMetadataToGateway init_value = WITH_METADATA_TO_GATEWAY__INIT;
  *message = init_value;
}
size_t with_metadata_to_gateway__get_packed_size
                     (const WithMetadataToGateway *message)
{
  assert(message->base.descriptor == &with_metadata_to_gateway__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t with_metadata_to_gateway__pack
                     (const WithMetadataToGateway *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &with_metadata_to_gateway__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t with_metadata_to_gateway__pack_to_buffer
                     (const WithMetadataToGateway *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &with_metadata_to_gateway__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
WithMetadataToGateway *
       with_metadata_to_gateway__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (WithMetadataToGateway *)
     protobuf_c_message_unpack (&with_metadata_to_gateway__descriptor,
                                allocator, len, data);
}
void   with_metadata_to_gateway__free_unpacked
                     (WithMetadataToGateway *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &with_metadata_to_gateway__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   with_metadata_from_gateway__init
                     (WithMetadataFromGateway         *message)
{
  static const WithMetadataFromGateway init_value = WITH_METADATA_FROM_GATEWAY__INIT;
  *message = init_value;
}
size_t with_metadata_from_gateway__get_packed_size
                     (const WithMetadataFromGateway *message)
{
  assert(message->base.descriptor == &with_metadata_from_gateway__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t with_metadata_from_gateway__pack
                     (const WithMetadataFromGateway *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &with_metadata_from_gateway__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t with_metadata_from_gateway__pack_to_buffer
                     (const WithMetadataFromGateway *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &with_metadata_from_gateway__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
WithMetadataFromGateway *
       with_metadata_from_gateway__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (WithMetadataFromGateway *)
     protobuf_c_message_unpack (&with_metadata_from_gateway__descriptor,
                                allocator, len, data);
}
void   with_metadata_from_gateway__free_unpacked
                     (WithMetadataFromGateway *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &with_metadata_from_gateway__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   with_metadata_to_device__init
                     (WithMetadataToDevice         *message)
{
  static const WithMetadataToDevice init_value = WITH_METADATA_TO_DEVICE__INIT;
  *message = init_value;
}
size_t with_metadata_to_device__get_packed_size
                     (const WithMetadataToDevice *message)
{
  assert(message->base.descriptor == &with_metadata_to_device__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t with_metadata_to_device__pack
                     (const WithMetadataToDevice *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &with_metadata_to_device__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t with_metadata_to_device__pack_to_buffer
                     (const WithMetadataToDevice *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &with_metadata_to_device__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
WithMetadataToDevice *
       with_metadata_to_device__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (WithMetadataToDevice *)
     protobuf_c_message_unpack (&with_metadata_to_device__descriptor,
                                allocator, len, data);
}
void   with_metadata_to_device__free_unpacked
                     (WithMetadataToDevice *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &with_metadata_to_device__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   gateway_metadata__init
                     (GatewayMetadata         *message)
{
  static const GatewayMetadata init_value = GATEWAY_METADATA__INIT;
  *message = init_value;
}
size_t gateway_metadata__get_packed_size
                     (const GatewayMetadata *message)
{
  assert(message->base.descriptor == &gateway_metadata__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t gateway_metadata__pack
                     (const GatewayMetadata *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &gateway_metadata__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t gateway_metadata__pack_to_buffer
                     (const GatewayMetadata *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &gateway_metadata__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
GatewayMetadata *
       gateway_metadata__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (GatewayMetadata *)
     protobuf_c_message_unpack (&gateway_metadata__descriptor,
                                allocator, len, data);
}
void   gateway_metadata__free_unpacked
                     (GatewayMetadata *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &gateway_metadata__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   device_metadata__init
                     (DeviceMetadata         *message)
{
  static const DeviceMetadata init_value = DEVICE_METADATA__INIT;
  *message = init_value;
}
size_t device_metadata__get_packed_size
                     (const DeviceMetadata *message)
{
  assert(message->base.descriptor == &device_metadata__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t device_metadata__pack
                     (const DeviceMetadata *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &device_metadata__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t device_metadata__pack_to_buffer
                     (const DeviceMetadata *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &device_metadata__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
DeviceMetadata *
       device_metadata__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (DeviceMetadata *)
     protobuf_c_message_unpack (&device_metadata__descriptor,
                                allocator, len, data);
}
void   device_metadata__free_unpacked
                     (DeviceMetadata *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &device_metadata__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
static const ProtobufCFieldDescriptor with_metadata_from_device__field_descriptors[9] =
{
  {
    "payload",
    1,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, payload),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "frequency",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, frequency),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "modulation",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, modulation),
    &modulation__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "bandwidth",
    4,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, bandwidth),
    &bandwidth__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "spreadingfactor",
    5,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, spreadingfactor),
    &spreading_factor__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "coderate",
    6,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, coderate),
    &code_rate__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payloadsize",
    7,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, payloadsize),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "transmitsignalstrength",
    8,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_DOUBLE,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, transmitsignalstrength),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "deveui",
    9,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromDevice, deveui),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned with_metadata_from_device__field_indices_by_name[] = {
  3,   /* field[3] = bandwidth */
  5,   /* field[5] = coderate */
  8,   /* field[8] = deveui */
  1,   /* field[1] = frequency */
  2,   /* field[2] = modulation */
  0,   /* field[0] = payload */
  6,   /* field[6] = payloadsize */
  4,   /* field[4] = spreadingfactor */
  7,   /* field[7] = transmitsignalstrength */
};
static const ProtobufCIntRange with_metadata_from_device__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 9 }
};
const ProtobufCMessageDescriptor with_metadata_from_device__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "WithMetadataFromDevice",
  "WithMetadataFromDevice",
  "WithMetadataFromDevice",
  "",
  sizeof(WithMetadataFromDevice),
  9,
  with_metadata_from_device__field_descriptors,
  with_metadata_from_device__field_indices_by_name,
  1,  with_metadata_from_device__number_ranges,
  (ProtobufCMessageInit) with_metadata_from_device__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor with_metadata_to_gateway__field_descriptors[8] =
{
  {
    "payload",
    1,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, payload),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "frequency",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, frequency),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "modulation",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, modulation),
    &modulation__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "bandwidth",
    4,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, bandwidth),
    &bandwidth__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "spreadingfactor",
    5,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, spreadingfactor),
    &spreading_factor__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "coderate",
    6,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, coderate),
    &code_rate__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payloadsize",
    7,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, payloadsize),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "signalstrength",
    8,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_DOUBLE,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToGateway, signalstrength),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned with_metadata_to_gateway__field_indices_by_name[] = {
  3,   /* field[3] = bandwidth */
  5,   /* field[5] = coderate */
  1,   /* field[1] = frequency */
  2,   /* field[2] = modulation */
  0,   /* field[0] = payload */
  6,   /* field[6] = payloadsize */
  7,   /* field[7] = signalstrength */
  4,   /* field[4] = spreadingfactor */
};
static const ProtobufCIntRange with_metadata_to_gateway__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 8 }
};
const ProtobufCMessageDescriptor with_metadata_to_gateway__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "WithMetadataToGateway",
  "WithMetadataToGateway",
  "WithMetadataToGateway",
  "",
  sizeof(WithMetadataToGateway),
  8,
  with_metadata_to_gateway__field_descriptors,
  with_metadata_to_gateway__field_indices_by_name,
  1,  with_metadata_to_gateway__number_ranges,
  (ProtobufCMessageInit) with_metadata_to_gateway__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor with_metadata_from_gateway__field_descriptors[9] =
{
  {
    "transmitsignalstrength",
    1,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_DOUBLE,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, transmitsignalstrength),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "modulation",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, modulation),
    &modulation__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "bandwidth",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, bandwidth),
    &bandwidth__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "spreadingfactor",
    4,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, spreadingfactor),
    &spreading_factor__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "coderate",
    5,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, coderate),
    &code_rate__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payload",
    6,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, payload),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payloadsize",
    7,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, payloadsize),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "frequency",
    8,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, frequency),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "macaddress",
    9,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataFromGateway, macaddress),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned with_metadata_from_gateway__field_indices_by_name[] = {
  2,   /* field[2] = bandwidth */
  4,   /* field[4] = coderate */
  7,   /* field[7] = frequency */
  8,   /* field[8] = macaddress */
  1,   /* field[1] = modulation */
  5,   /* field[5] = payload */
  6,   /* field[6] = payloadsize */
  3,   /* field[3] = spreadingfactor */
  0,   /* field[0] = transmitsignalstrength */
};
static const ProtobufCIntRange with_metadata_from_gateway__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 9 }
};
const ProtobufCMessageDescriptor with_metadata_from_gateway__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "WithMetadataFromGateway",
  "WithMetadataFromGateway",
  "WithMetadataFromGateway",
  "",
  sizeof(WithMetadataFromGateway),
  9,
  with_metadata_from_gateway__field_descriptors,
  with_metadata_from_gateway__field_indices_by_name,
  1,  with_metadata_from_gateway__number_ranges,
  (ProtobufCMessageInit) with_metadata_from_gateway__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor with_metadata_to_device__field_descriptors[8] =
{
  {
    "modulation",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, modulation),
    &modulation__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "bandwidth",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, bandwidth),
    &bandwidth__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "spreadingfactor",
    4,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, spreadingfactor),
    &spreading_factor__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "coderate",
    5,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_ENUM,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, coderate),
    &code_rate__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payload",
    6,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, payload),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "payloadsize",
    7,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, payloadsize),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "frequency",
    8,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_UINT32,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, frequency),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "signalstrength",
    9,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_DOUBLE,
    0,   /* quantifier_offset */
    offsetof(WithMetadataToDevice, signalstrength),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned with_metadata_to_device__field_indices_by_name[] = {
  1,   /* field[1] = bandwidth */
  3,   /* field[3] = coderate */
  6,   /* field[6] = frequency */
  0,   /* field[0] = modulation */
  4,   /* field[4] = payload */
  5,   /* field[5] = payloadsize */
  7,   /* field[7] = signalstrength */
  2,   /* field[2] = spreadingfactor */
};
static const ProtobufCIntRange with_metadata_to_device__number_ranges[1 + 1] =
{
  { 2, 0 },
  { 0, 8 }
};
const ProtobufCMessageDescriptor with_metadata_to_device__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "WithMetadataToDevice",
  "WithMetadataToDevice",
  "WithMetadataToDevice",
  "",
  sizeof(WithMetadataToDevice),
  8,
  with_metadata_to_device__field_descriptors,
  with_metadata_to_device__field_indices_by_name,
  1,  with_metadata_to_device__number_ranges,
  (ProtobufCMessageInit) with_metadata_to_device__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor gateway_metadata__field_descriptors[4] =
{
  {
    "macaddress",
    1,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(GatewayMetadata, macaddress),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "latitude",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_FLOAT,
    0,   /* quantifier_offset */
    offsetof(GatewayMetadata, latitude),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "longitude",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_FLOAT,
    0,   /* quantifier_offset */
    offsetof(GatewayMetadata, longitude),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "groups",
    4,
    PROTOBUF_C_LABEL_REPEATED,
    PROTOBUF_C_TYPE_UINT32,
    offsetof(GatewayMetadata, n_groups),
    offsetof(GatewayMetadata, groups),
    NULL,
    NULL,
    0 | PROTOBUF_C_FIELD_FLAG_PACKED,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned gateway_metadata__field_indices_by_name[] = {
  3,   /* field[3] = groups */
  1,   /* field[1] = latitude */
  2,   /* field[2] = longitude */
  0,   /* field[0] = macaddress */
};
static const ProtobufCIntRange gateway_metadata__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 4 }
};
const ProtobufCMessageDescriptor gateway_metadata__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "GatewayMetadata",
  "GatewayMetadata",
  "GatewayMetadata",
  "",
  sizeof(GatewayMetadata),
  4,
  gateway_metadata__field_descriptors,
  gateway_metadata__field_indices_by_name,
  1,  gateway_metadata__number_ranges,
  (ProtobufCMessageInit) gateway_metadata__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor device_metadata__field_descriptors[7] =
{
  {
    "deveui",
    1,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, deveui),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "latitude",
    2,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_FLOAT,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, latitude),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "longitude",
    3,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_FLOAT,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, longitude),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "devaddr",
    4,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, devaddr),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "nwkskey",
    5,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, nwkskey),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "appskey",
    6,
    PROTOBUF_C_LABEL_NONE,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(DeviceMetadata, appskey),
    NULL,
    &protobuf_c_empty_string,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "groups",
    7,
    PROTOBUF_C_LABEL_REPEATED,
    PROTOBUF_C_TYPE_UINT32,
    offsetof(DeviceMetadata, n_groups),
    offsetof(DeviceMetadata, groups),
    NULL,
    NULL,
    0 | PROTOBUF_C_FIELD_FLAG_PACKED,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned device_metadata__field_indices_by_name[] = {
  5,   /* field[5] = appskey */
  3,   /* field[3] = devaddr */
  0,   /* field[0] = deveui */
  6,   /* field[6] = groups */
  1,   /* field[1] = latitude */
  2,   /* field[2] = longitude */
  4,   /* field[4] = nwkskey */
};
static const ProtobufCIntRange device_metadata__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 7 }
};
const ProtobufCMessageDescriptor device_metadata__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "DeviceMetadata",
  "DeviceMetadata",
  "DeviceMetadata",
  "",
  sizeof(DeviceMetadata),
  7,
  device_metadata__field_descriptors,
  device_metadata__field_indices_by_name,
  1,  device_metadata__number_ranges,
  (ProtobufCMessageInit) device_metadata__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCEnumValue modulation__enum_values_by_number[3] =
{
  { "MOD_UNDEFINED", "MODULATION__MOD_UNDEFINED", 0 },
  { "MOD_LORA", "MODULATION__MOD_LORA", 1 },
  { "MOD_FSK", "MODULATION__MOD_FSK", 2 },
};
static const ProtobufCIntRange modulation__value_ranges[] = {
{0, 0},{0, 3}
};
static const ProtobufCEnumValueIndex modulation__enum_values_by_name[3] =
{
  { "MOD_FSK", 2 },
  { "MOD_LORA", 1 },
  { "MOD_UNDEFINED", 0 },
};
const ProtobufCEnumDescriptor modulation__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "Modulation",
  "Modulation",
  "Modulation",
  "",
  3,
  modulation__enum_values_by_number,
  3,
  modulation__enum_values_by_name,
  1,
  modulation__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
static const ProtobufCEnumValue bandwidth__enum_values_by_number[8] =
{
  { "BW_UNDEFINED", "BANDWIDTH__BW_UNDEFINED", 0 },
  { "BW_500KHZ", "BANDWIDTH__BW_500KHZ", 1 },
  { "BW_250KHZ", "BANDWIDTH__BW_250KHZ", 2 },
  { "BW_125KHZ", "BANDWIDTH__BW_125KHZ", 3 },
  { "BW_62K5HZ", "BANDWIDTH__BW_62K5HZ", 4 },
  { "BW_31K2HZ", "BANDWIDTH__BW_31K2HZ", 5 },
  { "BW_15K6HZ", "BANDWIDTH__BW_15K6HZ", 6 },
  { "BW_7K8HZ", "BANDWIDTH__BW_7K8HZ", 7 },
};
static const ProtobufCIntRange bandwidth__value_ranges[] = {
{0, 0},{0, 8}
};
static const ProtobufCEnumValueIndex bandwidth__enum_values_by_name[8] =
{
  { "BW_125KHZ", 3 },
  { "BW_15K6HZ", 6 },
  { "BW_250KHZ", 2 },
  { "BW_31K2HZ", 5 },
  { "BW_500KHZ", 1 },
  { "BW_62K5HZ", 4 },
  { "BW_7K8HZ", 7 },
  { "BW_UNDEFINED", 0 },
};
const ProtobufCEnumDescriptor bandwidth__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "Bandwidth",
  "Bandwidth",
  "Bandwidth",
  "",
  8,
  bandwidth__enum_values_by_number,
  8,
  bandwidth__enum_values_by_name,
  1,
  bandwidth__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
static const ProtobufCEnumValue spreading_factor__enum_values_by_number[7] =
{
  { "DR_UNDEFINED", "SPREADING_FACTOR__DR_UNDEFINED", 0 },
  { "SF7", "SPREADING_FACTOR__SF7", 2 },
  { "SF8", "SPREADING_FACTOR__SF8", 4 },
  { "SF9", "SPREADING_FACTOR__SF9", 8 },
  { "SF10", "SPREADING_FACTOR__SF10", 16 },
  { "SF11", "SPREADING_FACTOR__SF11", 32 },
  { "SF12", "SPREADING_FACTOR__SF12", 64 },
};
static const ProtobufCIntRange spreading_factor__value_ranges[] = {
{0, 0},{2, 1},{4, 2},{8, 3},{16, 4},{32, 5},{64, 6},{0, 7}
};
static const ProtobufCEnumValueIndex spreading_factor__enum_values_by_name[7] =
{
  { "DR_UNDEFINED", 0 },
  { "SF10", 4 },
  { "SF11", 5 },
  { "SF12", 6 },
  { "SF7", 1 },
  { "SF8", 2 },
  { "SF9", 3 },
};
const ProtobufCEnumDescriptor spreading_factor__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "SpreadingFactor",
  "SpreadingFactor",
  "SpreadingFactor",
  "",
  7,
  spreading_factor__enum_values_by_number,
  7,
  spreading_factor__enum_values_by_name,
  7,
  spreading_factor__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
static const ProtobufCEnumValue code_rate__enum_values_by_number[5] =
{
  { "CR_UNDEFINED", "CODE_RATE__CR_UNDEFINED", 0 },
  { "CR_LORA_4_5", "CODE_RATE__CR_LORA_4_5", 1 },
  { "CR_LORA_4_6", "CODE_RATE__CR_LORA_4_6", 2 },
  { "CR_LORA_4_7", "CODE_RATE__CR_LORA_4_7", 3 },
  { "CR_LORA_4_8", "CODE_RATE__CR_LORA_4_8", 4 },
};
static const ProtobufCIntRange code_rate__value_ranges[] = {
{0, 0},{0, 5}
};
static const ProtobufCEnumValueIndex code_rate__enum_values_by_name[5] =
{
  { "CR_LORA_4_5", 1 },
  { "CR_LORA_4_6", 2 },
  { "CR_LORA_4_7", 3 },
  { "CR_LORA_4_8", 4 },
  { "CR_UNDEFINED", 0 },
};
const ProtobufCEnumDescriptor code_rate__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "CodeRate",
  "CodeRate",
  "CodeRate",
  "",
  5,
  code_rate__enum_values_by_number,
  5,
  code_rate__enum_values_by_name,
  1,
  code_rate__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
