/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2013 Semtech-Cycleo

Description:
    LoRa concentrator Hardware Abstraction Layer

License: Revised BSD License, see LICENSE.TXT file include in the project
Maintainer: Sylvain Miermont
*/

/* -------------------------------------------------------------------------- */
/* --- DEPENDANCIES --------------------------------------------------------- */

#define _GNU_SOURCE
#include <math.h>    /* pow, cell */
#include <stdbool.h> /* bool type */
#include <stdint.h>  /* C99 types */
#include <stdio.h>   /* printf fprintf */
#include <string.h>  /* memcpy */

#include "loragw_aux.h"
#include "loragw_fpga.h"
#include "loragw_hal.h"
#include "loragw_lbt.h"
#include "loragw_radio.h"
#include "loragw_reg.h"
#include "loragw_spi.h"

/* -------------------------------------------------------------------------- */
/* --- PRIVATE MACROS ------------------------------------------------------- */

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#if DEBUG_HAL == 1
#define DEBUG_MSG(str) fprintf(stderr, str)
#define DEBUG_PRINTF(fmt, args...)                                             \
  fprintf(stderr, "%s:%d: " fmt, __FUNCTION__, __LINE__, args)
#define DEBUG_ARRAY(a, b, c)                                                   \
  for (a = 0; a < b; ++a)                                                      \
    fprintf(stderr, "%x.", c[a]);                                              \
  fprintf(stderr, "end\n")
#define CHECK_NULL(a)                                                          \
  if (a == NULL) {                                                             \
    fprintf(stderr, "%s:%d: ERROR: NULL POINTER AS ARGUMENT\n", __FUNCTION__,  \
            __LINE__);                                                         \
    return LGW_HAL_ERROR;                                                      \
  }
#else
#define DEBUG_MSG(str)
#define DEBUG_PRINTF(fmt, args...)
#define DEBUG_ARRAY(a, b, c)                                                   \
  for (a = 0; a != 0;) {                                                       \
  }
#define CHECK_NULL(a)                                                          \
  if (a == NULL) {                                                             \
    return LGW_HAL_ERROR;                                                      \
  }
#endif

#define IF_HZ_TO_REG(f) (f << 5) / 15625
#define SET_PPM_ON(bw, dr)                                                     \
  (((bw == BW_125KHZ) && ((dr == DR_LORA_SF11) || (dr == DR_LORA_SF12))) ||    \
   ((bw == BW_250KHZ) && (dr == DR_LORA_SF12)))
#define TRACE() fprintf(stderr, "@ %s %d\n", __FUNCTION__, __LINE__);

/* -------------------------------------------------------------------------- */
/* --- PRIVATE CONSTANTS & TYPES -------------------------------------------- */

/* Useful bandwidth of SX125x radios to consider depending on channel bandwidth
 */
/* Note: the below values come from lab measurements. For any question, please
 * contact Semtech support */
#define LGW_RF_RX_BANDWIDTH_125KHZ 925000  /* for 125KHz channels */
#define LGW_RF_RX_BANDWIDTH_250KHZ 1000000 /* for 250KHz channels */
#define LGW_RF_RX_BANDWIDTH_500KHZ 1100000 /* for 500KHz channels */

#define TX_START_DELAY_DEFAULT                                                 \
  1497 /* Calibrated value for 500KHz BW and notch filter disabled */

/* constant arrays defining hardware capability */
const uint8_t ifmod_config[LGW_IF_CHAIN_NB] = LGW_IFMODEM_CONFIG;

/* Version string, used to identify the library version/options once compiled */
const char lgw_version_string[] = "Version: " LIBLORAGW_VERSION ";";

/* -------------------------------------------------------------------------- */
/* --- PRIVATE VARIABLES ---------------------------------------------------- */

/*
The following static variables are the configuration set that the user can
modify using rxrf_setconf, rxif_setconf and txgain_setconf functions.
The functions _start and _send then use that set to configure the hardware.

Parameters validity and coherency is verified by the _setconf functions and
the _start and _send functions assume they are valid.
*/

static volatile bool started = false;
static bool rf_enable[LGW_RF_CHAIN_NB];
static uint32_t rf_rx_freq[LGW_RF_CHAIN_NB]; /* absolute, in Hz */
static float rf_rssi_offset[LGW_RF_CHAIN_NB];
static bool rf_tx_enable[LGW_RF_CHAIN_NB];
static uint32_t rf_tx_notch_freq[LGW_RF_CHAIN_NB];
static enum lgw_radio_type_e rf_radio_type[LGW_RF_CHAIN_NB];

static bool if_enable[LGW_IF_CHAIN_NB];
static bool
    if_rf_chain[LGW_IF_CHAIN_NB]; /* for each IF, 0 -> radio A, 1 -> radio B */
static int32_t
    if_freq[LGW_IF_CHAIN_NB]; /* relative to radio frequency, +/- in Hz */

static uint8_t
    lora_multi_sfmask[LGW_MULTI_NB]; /* enables SF for LoRa 'multi' modems */

static uint8_t lora_rx_bw; /* bandwidth setting for LoRa standalone modem */
static uint8_t
    lora_rx_sf; /* spreading factor setting for LoRa standalone modem */
static bool lora_rx_ppm_offset;

static uint8_t fsk_rx_bw;  /* bandwidth setting of FSK modem */
static uint32_t fsk_rx_dr; /* FSK modem datarate in bauds */
static uint8_t fsk_sync_word_size =
    3; /* default number of bytes for FSK sync word */
static uint64_t fsk_sync_word =
    0xC194C1; /* default FSK sync word (ALIGNED RIGHT, MSbit first) */

static bool lorawan_public = false;
static uint8_t rf_clkout = 0;

static struct lgw_tx_gain_lut_s txgain_lut = {.size = 2,
                                              .lut[0] = {.dig_gain = 0,
                                                         .pa_gain = 2,
                                                         .dac_gain = 3,
                                                         .mix_gain = 10,
                                                         .rf_power = 14},
                                              .lut[1] = {.dig_gain = 0,
                                                         .pa_gain = 3,
                                                         .dac_gain = 3,
                                                         .mix_gain = 14,
                                                         .rf_power = 27}};

/* -------------------------------------------------------------------------- */
/* --- PRIVATE FUNCTIONS DEFINITION ----------------------------------------- */

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int32_t lgw_bw_getval(int x) {
  switch (x) {
  case BW_500KHZ:
    return 500000;
  case BW_250KHZ:
    return 250000;
  case BW_125KHZ:
    return 125000;
  case BW_62K5HZ:
    return 62500;
  case BW_31K2HZ:
    return 31200;
  case BW_15K6HZ:
    return 15600;
  case BW_7K8HZ:
    return 7800;
  default:
    return -1;
  }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int32_t lgw_sf_getval(int x) {
  switch (x) {
  case DR_LORA_SF7:
    return 7;
  case DR_LORA_SF8:
    return 8;
  case DR_LORA_SF9:
    return 9;
  case DR_LORA_SF10:
    return 10;
  case DR_LORA_SF11:
    return 11;
  case DR_LORA_SF12:
    return 12;
  default:
    return -1;
  }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

uint16_t lgw_get_tx_start_delay(bool tx_notch_enable, uint8_t bw) {
  float notch_delay_us = 0.0;
  float bw_delay_us = 0.0;
  float tx_start_delay;

  /* Notch filtering performed by FPGA adds a constant delay (group delay) that
   * we need to compensate */
  if (tx_notch_enable) {
    notch_delay_us = lgw_fpga_get_tx_notch_delay();
  }

  /* Calibrated delay brought by SX1301 depending on signal bandwidth */
  switch (bw) {
  case BW_125KHZ:
    bw_delay_us = 1.5;
    break;
  case BW_500KHZ:
    /* Intended fall-through: it is the calibrated reference */
  default:
    break;
  }

  tx_start_delay = (float)TX_START_DELAY_DEFAULT - bw_delay_us - notch_delay_us;

  printf("INFO: tx_start_delay=%u (%f) - (%u, bw_delay=%f, notch_delay=%f)\n",
         (uint16_t)tx_start_delay, tx_start_delay, TX_START_DELAY_DEFAULT,
         bw_delay_us, notch_delay_us);

  return (uint16_t)tx_start_delay; /* keep truncating instead of rounding:
                                      better behaviour measured */
}

/* -------------------------------------------------------------------------- */
/* --- PUBLIC FUNCTIONS DEFINITION ------------------------------------------ */

int lgw_board_setconf(struct lgw_conf_board_s conf) {

  /* check if the concentrator is running */
  if (started == true) {
    DEBUG_MSG("ERROR: CONCENTRATOR IS RUNNING, STOP IT BEFORE TOUCHING "
              "CONFIGURATION\n");
    return LGW_HAL_ERROR;
  }

  /* set internal config according to parameters */
  lorawan_public = conf.lorawan_public;
  rf_clkout = conf.clksrc;

  DEBUG_PRINTF("Note: board configuration; lorawan_public:%d, clksrc:%d\n",
               lorawan_public, rf_clkout);

  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_lbt_setconf(struct lgw_conf_lbt_s conf) {
  int x;

  /* check if the concentrator is running */
  if (started == true) {
    DEBUG_MSG("ERROR: CONCENTRATOR IS RUNNING, STOP IT BEFORE TOUCHING "
              "CONFIGURATION\n");
    return LGW_HAL_ERROR;
  }

  x = lbt_setconf(&conf);
  if (x != LGW_LBT_SUCCESS) {
    DEBUG_MSG("ERROR: Failed to configure concentrator for LBT\n");
    return LGW_HAL_ERROR;
  }

  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_rxrf_setconf(uint8_t rf_chain, struct lgw_conf_rxrf_s conf) {

  /* check if the concentrator is running */
  if (started == true) {
    DEBUG_MSG("ERROR: CONCENTRATOR IS RUNNING, STOP IT BEFORE TOUCHING "
              "CONFIGURATION\n");
    return LGW_HAL_ERROR;
  }

  /* check input range (segfault prevention) */
  if (rf_chain >= LGW_RF_CHAIN_NB) {
    DEBUG_MSG("ERROR: NOT A VALID RF_CHAIN NUMBER\n");
    return LGW_HAL_ERROR;
  }

  /* check if radio type is supported */
  if ((conf.type != LGW_RADIO_TYPE_SX1255) &&
      (conf.type != LGW_RADIO_TYPE_SX1257)) {
    DEBUG_MSG("ERROR: NOT A VALID RADIO TYPE\n");
    return LGW_HAL_ERROR;
  }

  /* check if TX notch filter frequency is supported */
  if ((conf.tx_enable == true) && ((conf.tx_notch_freq < LGW_MIN_NOTCH_FREQ) ||
                                   (conf.tx_notch_freq > LGW_MAX_NOTCH_FREQ))) {
    DEBUG_PRINTF("WARNING: NOT A VALID TX NOTCH FILTER FREQUENCY [%u..%u]Hz\n",
                 LGW_MIN_NOTCH_FREQ, LGW_MAX_NOTCH_FREQ);
    conf.tx_notch_freq = 0;
  }

  /* set internal config according to parameters */
  rf_enable[rf_chain] = conf.enable;
  rf_rx_freq[rf_chain] = conf.freq_hz;
  rf_rssi_offset[rf_chain] = conf.rssi_offset;
  rf_radio_type[rf_chain] = conf.type;
  rf_tx_enable[rf_chain] = conf.tx_enable;
  rf_tx_notch_freq[rf_chain] = conf.tx_notch_freq;

  DEBUG_PRINTF("Note: rf_chain %d configuration; en:%d freq:%d rssi_offset:%f "
               "radio_type:%d tx_enable:%d tx_notch_freq:%u\n",
               rf_chain, rf_enable[rf_chain], rf_rx_freq[rf_chain],
               rf_rssi_offset[rf_chain], rf_radio_type[rf_chain],
               rf_tx_enable[rf_chain], rf_tx_notch_freq[rf_chain]);

  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_rxif_setconf(uint8_t if_chain, struct lgw_conf_rxif_s conf) {
  int32_t bw_hz;
  uint32_t rf_rx_bandwidth;

  /* check if the concentrator is running */
  if (started == true) {
    DEBUG_MSG("ERROR: CONCENTRATOR IS RUNNING, STOP IT BEFORE TOUCHING "
              "CONFIGURATION\n");
    return LGW_HAL_ERROR;
  }

  /* check input range (segfault prevention) */
  if (if_chain >= LGW_IF_CHAIN_NB) {
    DEBUG_PRINTF("ERROR: %d NOT A VALID IF_CHAIN NUMBER\n", if_chain);
    return LGW_HAL_ERROR;
  }

  /* if chain is disabled, don't care about most parameters */
  if (conf.enable == false) {
    if_enable[if_chain] = false;
    if_freq[if_chain] = 0;
    DEBUG_PRINTF("Note: if_chain %d disabled\n", if_chain);
    return LGW_HAL_SUCCESS;
  }

  /* check 'general' parameters */
  if (ifmod_config[if_chain] == IF_UNDEFINED) {
    DEBUG_PRINTF("ERROR: IF CHAIN %d NOT CONFIGURABLE\n", if_chain);
  }
  if (conf.rf_chain >= LGW_RF_CHAIN_NB) {
    DEBUG_MSG(
        "ERROR: INVALID RF_CHAIN TO ASSOCIATE WITH A LORA_STD IF CHAIN\n");
    return LGW_HAL_ERROR;
  }
  /* check if IF frequency is optimal based on channel and radio bandwidths */
  switch (conf.bandwidth) {
  case BW_250KHZ:
    rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_250KHZ; /* radio bandwidth */
    break;
  case BW_500KHZ:
    rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_500KHZ; /* radio bandwidth */
    break;
  default:
    /* For 125KHz and below */
    rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_125KHZ; /* radio bandwidth */
    break;
  }
  bw_hz = lgw_bw_getval(conf.bandwidth); /* channel bandwidth */
  if ((conf.freq_hz + ((bw_hz == -1) ? LGW_REF_BW : bw_hz) / 2) >
      ((int32_t)rf_rx_bandwidth / 2)) {
    DEBUG_PRINTF("ERROR: IF FREQUENCY %d TOO HIGH\n", conf.freq_hz);
    return LGW_HAL_ERROR;
  } else if ((conf.freq_hz - ((bw_hz == -1) ? LGW_REF_BW : bw_hz) / 2) <
             -((int32_t)rf_rx_bandwidth / 2)) {
    DEBUG_PRINTF("ERROR: IF FREQUENCY %d TOO LOW\n", conf.freq_hz);
    return LGW_HAL_ERROR;
  }

  /* check parameters according to the type of IF chain + modem,
  fill default if necessary, and commit configuration if everything is OK */
  switch (ifmod_config[if_chain]) {
  case IF_LORA_STD:
    /* fill default parameters if needed */
    if (conf.bandwidth == BW_UNDEFINED) {
      conf.bandwidth = BW_250KHZ;
    }
    if (conf.datarate == DR_UNDEFINED) {
      conf.datarate = DR_LORA_SF9;
    }
    /* check BW & DR */
    if (!IS_LORA_BW(conf.bandwidth)) {
      DEBUG_MSG("ERROR: BANDWIDTH NOT SUPPORTED BY LORA_STD IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    if (!IS_LORA_STD_DR(conf.datarate)) {
      DEBUG_MSG("ERROR: DATARATE NOT SUPPORTED BY LORA_STD IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    /* set internal configuration  */
    if_enable[if_chain] = conf.enable;
    if_rf_chain[if_chain] = conf.rf_chain;
    if_freq[if_chain] = conf.freq_hz;
    lora_rx_bw = conf.bandwidth;
    lora_rx_sf = (uint8_t)(DR_LORA_MULTI &
                           conf.datarate); /* filter SF out of the 7-12 range */
    if (SET_PPM_ON(conf.bandwidth, conf.datarate)) {
      lora_rx_ppm_offset = true;
    } else {
      lora_rx_ppm_offset = false;
    }

    DEBUG_PRINTF("Note: LoRa 'std' if_chain %d configuration; en:%d freq:%d "
                 "bw:%d dr:%d\n",
                 if_chain, if_enable[if_chain], if_freq[if_chain], lora_rx_bw,
                 lora_rx_sf);
    break;

  case IF_LORA_MULTI:
    /* fill default parameters if needed */
    if (conf.bandwidth == BW_UNDEFINED) {
      conf.bandwidth = BW_125KHZ;
    }
    if (conf.datarate == DR_UNDEFINED) {
      conf.datarate = DR_LORA_MULTI;
    }
    /* check BW & DR */
    if (conf.bandwidth != BW_125KHZ) {
      DEBUG_MSG("ERROR: BANDWIDTH NOT SUPPORTED BY LORA_MULTI IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    if (!IS_LORA_MULTI_DR(conf.datarate)) {
      DEBUG_MSG("ERROR: DATARATE(S) NOT SUPPORTED BY LORA_MULTI IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    /* set internal configuration  */
    if_enable[if_chain] = conf.enable;
    if_rf_chain[if_chain] = conf.rf_chain;
    if_freq[if_chain] = conf.freq_hz;
    lora_multi_sfmask[if_chain] = (uint8_t)(
        DR_LORA_MULTI & conf.datarate); /* filter SF out of the 7-12 range */

    DEBUG_PRINTF("Note: LoRa 'multi' if_chain %d configuration; en:%d freq:%d "
                 "SF_mask:0x%02x\n",
                 if_chain, if_enable[if_chain], if_freq[if_chain],
                 lora_multi_sfmask[if_chain]);
    break;

  case IF_FSK_STD:
    /* fill default parameters if needed */
    if (conf.bandwidth == BW_UNDEFINED) {
      conf.bandwidth = BW_250KHZ;
    }
    if (conf.datarate == DR_UNDEFINED) {
      conf.datarate = 64000; /* default datarate */
    }
    /* check BW & DR */
    if (!IS_FSK_BW(conf.bandwidth)) {
      DEBUG_MSG("ERROR: BANDWIDTH NOT SUPPORTED BY FSK IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    if (!IS_FSK_DR(conf.datarate)) {
      DEBUG_MSG("ERROR: DATARATE NOT SUPPORTED BY FSK IF CHAIN\n");
      return LGW_HAL_ERROR;
    }
    /* set internal configuration  */
    if_enable[if_chain] = conf.enable;
    if_rf_chain[if_chain] = conf.rf_chain;
    if_freq[if_chain] = conf.freq_hz;
    fsk_rx_bw = conf.bandwidth;
    fsk_rx_dr = conf.datarate;
    if (conf.sync_word > 0) {
      fsk_sync_word_size = conf.sync_word_size;
      fsk_sync_word = conf.sync_word;
    }
    DEBUG_PRINTF("Note: FSK if_chain %d configuration; en:%d freq:%d bw:%d "
                 "dr:%d (%d real dr) sync:0x%0*llX\n",
                 if_chain, if_enable[if_chain], if_freq[if_chain], fsk_rx_bw,
                 fsk_rx_dr, LGW_XTAL_FREQU / (LGW_XTAL_FREQU / fsk_rx_dr),
                 2 * fsk_sync_word_size, fsk_sync_word);
    break;

  default:
    DEBUG_PRINTF("ERROR: IF CHAIN %d TYPE NOT SUPPORTED\n", if_chain);
    return LGW_HAL_ERROR;
  }

  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_txgain_setconf(struct lgw_tx_gain_lut_s *conf) {
  int i;

  /* Check LUT size */
  if ((conf->size < 1) || (conf->size > TX_GAIN_LUT_SIZE_MAX)) {
    DEBUG_PRINTF("ERROR: TX gain LUT must have at least one entry and  maximum "
                 "%d entries\n",
                 TX_GAIN_LUT_SIZE_MAX);
    return LGW_HAL_ERROR;
  }

  txgain_lut.size = conf->size;

  for (i = 0; i < txgain_lut.size; i++) {
    /* Check gain range */
    if (conf->lut[i].dig_gain > 3) {
      DEBUG_MSG(
          "ERROR: TX gain LUT: SX1301 digital gain must be between 0 and 3\n");
      return LGW_HAL_ERROR;
    }
    if (conf->lut[i].dac_gain != 3) {
      DEBUG_MSG(
          "ERROR: TX gain LUT: SX1257 DAC gains != 3 are not supported\n");
      return LGW_HAL_ERROR;
    }
    if (conf->lut[i].mix_gain > 15) {
      DEBUG_MSG("ERROR: TX gain LUT: SX1257 mixer gain must not exceed 15\n");
      return LGW_HAL_ERROR;
    } else if (conf->lut[i].mix_gain < 8) {
      DEBUG_MSG(
          "ERROR: TX gain LUT: SX1257 mixer gains < 8 are not supported\n");
      return LGW_HAL_ERROR;
    }
    if (conf->lut[i].pa_gain > 3) {
      DEBUG_MSG("ERROR: TX gain LUT: External PA gain must not exceed 3\n");
      return LGW_HAL_ERROR;
    }

    /* Set internal LUT */
    txgain_lut.lut[i].dig_gain = conf->lut[i].dig_gain;
    txgain_lut.lut[i].dac_gain = conf->lut[i].dac_gain;
    txgain_lut.lut[i].mix_gain = conf->lut[i].mix_gain;
    txgain_lut.lut[i].pa_gain = conf->lut[i].pa_gain;
    txgain_lut.lut[i].rf_power = conf->lut[i].rf_power;
  }

  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

#include <sys/time.h>
#include <time.h>
uint32_t now_microseconds(void) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (uint32_t)((uint64_t)tv.tv_sec * 1000000 + (uint64_t)tv.tv_usec);
}
#include "uplink.pb-c.h"
#include <amqp.h>
#include <amqp_tcp_socket.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

int check_amqp_error(amqp_rpc_reply_t x, char const *context) {
  switch (x.reply_type) {
  case AMQP_RESPONSE_NORMAL:
    return LGW_HAL_SUCCESS;

  case AMQP_RESPONSE_NONE:
    fprintf(stderr, "%s: missing RPC reply type!\n", context);
    return LGW_HAL_ERROR;

  case AMQP_RESPONSE_LIBRARY_EXCEPTION:
    fprintf(stderr, "%s: %s\n", context, amqp_error_string2(x.library_error));
    return LGW_HAL_ERROR;

  case AMQP_RESPONSE_SERVER_EXCEPTION:
    switch (x.reply.id) {
    case AMQP_CONNECTION_CLOSE_METHOD: {
      amqp_connection_close_t *m = (amqp_connection_close_t *)x.reply.decoded;
      fprintf(stderr, "%s: server connection error %uh, message: %.*s\n",
              context, m->reply_code, (int)m->reply_text.len,
              (char *)m->reply_text.bytes);
      return LGW_HAL_ERROR;
    }
    case AMQP_CHANNEL_CLOSE_METHOD: {
      amqp_channel_close_t *m = (amqp_channel_close_t *)x.reply.decoded;
      fprintf(stderr, "%s: server channel error %uh, message: %.*s\n", context,
              m->reply_code, (int)m->reply_text.len,
              (char *)m->reply_text.bytes);
      return LGW_HAL_ERROR;
    }
    default:
      fprintf(stderr, "%s: unknown server error, method id 0x%08X\n", context,
              x.reply.id);
      return LGW_HAL_ERROR;
    }
    break;
  }
  return LGW_HAL_SUCCESS;
}

enum duplexity { FULL_DUPLEX, HALF_DUPLEX };
enum radiomodel { SIMPLE };

typedef struct config {
  char *networkinterfacename;
  char *macaddress;
  char *rabbitmqurl;
  char *rabbitmqusername;
  char *rabbitmqpassword;
  char *devupchannelname;
  char *gwdnchannelname;
  int rabbitmqport;
  double longitude;
  double latitude;
  char *envhost;
  uint envport;
  enum duplexity duplexity;
  char *lorioturl;
  double expectedrssi;
  double rssibias;
  double expectedsnr;
  double snrbias;
  enum radiomodel radio_model;
} config_t;

config_t config;

amqp_connection_state_t conn_rec;
amqp_connection_state_t conn_send;
amqp_socket_t *socket_rec;
amqp_socket_t *socket_send;
static pthread_t amqp_receive_thread;
static pthread_mutex_t amqp_receive_lock;
static bool transmitting = false;
struct lgw_pkt_rx_s *uplinks[16];
uint32_t start = 0;

float calculaterssi(/*double signal*/) {
  switch (config.radio_model) {
  case SIMPLE:;
    double num0_2bias =
        (double)rand() / (double)RAND_MAX * (config.rssibias * 2);
    return config.expectedrssi + num0_2bias - config.rssibias;
  default:
    return config.expectedrssi;
  }
}

float calculatesnr(/*double signal*/) {
  switch (config.radio_model) {
  case SIMPLE:;
    double num0_2bias =
        (double)rand() / (double)RAND_MAX * (config.snrbias * 2);
    return config.expectedsnr + num0_2bias - config.snrbias;
  default:
    return config.expectedsnr;
  }
}

void amqp_receive() {
  while (true) {
    amqp_rpc_reply_t res;
    amqp_envelope_t envelope;

    amqp_maybe_release_buffers(conn_rec);

    res = amqp_consume_message(conn_rec, &envelope, NULL, 0);

    if (AMQP_RESPONSE_NORMAL != res.reply_type) {
      continue;
    }

    WithMetadataToGateway *msg = with_metadata_to_gateway__unpack(
        NULL, envelope.message.body.len,
        (unsigned char *)envelope.message.body.bytes);
    if (msg == NULL) {
      printf("warning, msg is null\n");
      continue;
    } else {
      if (config.duplexity == HALF_DUPLEX && transmitting) {
        printf("dropping uplink because device is half-duplex\n");
        continue;
      }
      bool added = false;
      pthread_mutex_lock(&amqp_receive_lock);
      for (size_t i = 0; i < 16; i++) {
        if (uplinks[i] != NULL) {
          continue;
        } else {
          struct lgw_pkt_rx_s *u = calloc(1, sizeof(struct lgw_pkt_rx_s));
          u->bandwidth = msg->bandwidth;
          u->coderate = msg->coderate;
          u->freq_hz = msg->frequency;
          if (msg->modulation == MODULATION__MOD_LORA) {
            u->modulation = MOD_LORA;
          } else if (msg->modulation == MODULATION__MOD_FSK) {
            u->modulation = MOD_FSK;
          } else {
            u->modulation = MOD_UNDEFINED;
          }
          unsigned int *bytearray =
              malloc(sizeof(unsigned int) * msg->payloadsize);
          for (uint32_t i = 0; i < (msg->payloadsize); i++) {
            sscanf(msg->payload + 2 * i, "%02x", &bytearray[i]);
            u->payload[i] = (uint8_t)bytearray[i];
          }

          free(bytearray);
          uint32_t tmst;
          lgw_get_trigcnt(&tmst);
          u->datarate = msg->spreadingfactor;
          u->status = STAT_CRC_OK;
          u->size = msg->payloadsize;
          u->if_chain = 0;
          u->rf_chain = 0;
          u->rssi = calculaterssi();
          u->snr = calculatesnr();
          u->snr_min = u->snr - 1;
          u->snr_max = u->snr + 1;
          u->count_us = tmst;
          uplinks[i] = u;
          added = true;
          break;
        }
      }
      amqp_basic_ack(conn_rec, 1, envelope.delivery_tag, false);
      int r = check_amqp_error(amqp_get_rpc_reply(conn_rec), "basic ack");
      if (r != 0) {
        printf("ACK of incoming message failed\n");
      }
      pthread_mutex_unlock(&amqp_receive_lock);
      if (!added) {
        printf("uplink queue is full, uplink dropped\n");
      }
    }
    with_metadata_to_gateway__free_unpacked(msg, NULL);
    amqp_destroy_envelope(&envelope);
  }
}
/* Generic */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Network */
#include <netdb.h>
#include <sys/socket.h>

// Get host information (used to establishConnection)
struct addrinfo *getHostInfo(char *host, char *port) {
  int r;
  struct addrinfo hints, *getaddrinfo_res;
  // Setup hints
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if ((r = getaddrinfo(host, port, &hints, &getaddrinfo_res))) {
    fprintf(stderr, "gethostinfo: %s\n", gai_strerror(r));
    return NULL;
  }

  return getaddrinfo_res;
}

// Establish connection with host
int establishConnection(struct addrinfo *info) {
  if (info == NULL)
    return -1;

  int clientfd;
  for (; info != NULL; info = info->ai_next) {
    if ((clientfd = socket(info->ai_family, info->ai_socktype,
                           info->ai_protocol)) < 0) {
      perror("[establishConnection:35:socket]");
      continue;
    }

    if (connect(clientfd, info->ai_addr, info->ai_addrlen) < 0) {
      close(clientfd);
      perror("[establishConnection:42:connect]");
      continue;
    }

    freeaddrinfo(info);
    return clientfd;
  }

  freeaddrinfo(info);
  return -1;
}

void POST(int clientfd, char *path, char *content, int length) {
  char req[2000] = {0};
  sprintf(req, "POST %s HTTP/1.0\r\nContent-Length: %d\r\n\r\n%s", path, length,
          content);
  ssize_t r = send(clientfd, req, strlen(req), 0);
  if (r < 0) {
    fprintf(stderr, "post: %s\n", gai_strerror(r));
  }
}
#include <yaml.h>

// this value must be changed when config is extended
const int numberoffields = 20;
int fieldcounter = 0;

int lgw_start(void) {
  printf("starting.\n");
  srand(time(NULL));
  start = now_microseconds();
  fieldcounter = 0;
  transmitting = false;
  for (size_t i = 0; i < 16; i++) {
    uplinks[i] = NULL;
  }

  FILE *fh = fopen("config.yml", "r");
  if (fh == NULL) {
    printf("Failed to open file!\n");
    return LGW_HAL_ERROR;
  }
  yaml_parser_t parser;
  yaml_event_t event;
  if (!yaml_parser_initialize(&parser)) {
    printf("Failed to initialize parser!\n");
    return LGW_HAL_ERROR;
  }
  if (fh == NULL) {
    printf("Failed to open file!\n");
    return LGW_HAL_ERROR;
  }
  yaml_parser_set_input_file(&parser, fh);
  bool value = false;
  bool seq = false;
  char name[256];
  char seqname[256];
  int groups[256];
  int grouplen = 0;
  do {
    if (!yaml_parser_parse(&parser, &event)) {
      printf("Parser error %d\n", parser.error);
      return LGW_HAL_ERROR;
    }
    switch (event.type) {
    case YAML_NO_EVENT:
      break;
    /* Stream start/end */
    case YAML_STREAM_START_EVENT:
      break;
    case YAML_STREAM_END_EVENT:
      break;
    /* Block delimeters */
    case YAML_DOCUMENT_START_EVENT:
      break;
    case YAML_DOCUMENT_END_EVENT:
      break;
    case YAML_SEQUENCE_START_EVENT:
      seq = true;
      break;
    case YAML_SEQUENCE_END_EVENT:
      seq = false;
      value = false;
      break;
    case YAML_MAPPING_START_EVENT:
      value = false;
      break;
    case YAML_MAPPING_END_EVENT:
      break;
    case YAML_ALIAS_EVENT:
      break;
    case YAML_SCALAR_EVENT:
      if (seq) {
        if (!strcmp(seqname, "groups")) {
          int i = atoi((char *)event.data.scalar.value);
          groups[grouplen] = i;
          grouplen++;
        }
      } else if (value) {
        // printf("%s: %s\n",name, event.data.scalar.value);
        if (!strcmp(name, "networkinterfacename")) {
          config.networkinterfacename =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.networkinterfacename, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "macaddress")) {
          config.macaddress =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.macaddress, (char *)event.data.scalar.value);
          config.macaddress[17] = 0;
          fieldcounter++;
        } else if (!strcmp(name, "rabbitmqserverurl")) {
          config.rabbitmqurl =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.rabbitmqurl, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "rabbitmqusername")) {
          config.rabbitmqusername =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.rabbitmqusername, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "rabbitmqpassword")) {
          config.rabbitmqpassword =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.rabbitmqpassword, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "rabbitmqport")) {
          config.rabbitmqport = atoi((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "deviceupqueuename")) {
          config.devupchannelname =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.devupchannelname, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "gatewaydownqueuename")) {
          config.gwdnchannelname =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.gwdnchannelname, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "longitude")) {
          config.longitude = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "latitude")) {
          config.latitude = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "environmentserverurl")) {
          config.envhost = malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.envhost, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "environmentserverport")) {
          config.envport = (uint)atoi((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "duplexity")) {
          if (strcmp((char *)event.data.scalar.value, "fullduplex") == 0) {
            config.duplexity = FULL_DUPLEX;
          } else if (strcmp((char *)event.data.scalar.value, "halfduplex") ==
                     0) {
            config.duplexity = HALF_DUPLEX;
          } else {
            config.duplexity = FULL_DUPLEX;
          }
          fieldcounter++;
        } else if (!strcmp(name, "radiomodel")) {
          if (strcmp((char *)event.data.scalar.value, "simple") == 0) {
            config.radio_model = SIMPLE;
          } else {
            config.radio_model = SIMPLE;
          }
          fieldcounter++;
        } else if (!strcmp(name, "loriotserverurl")) {
          config.lorioturl =
              malloc(strlen((char *)event.data.scalar.value) + 1);
          strcpy(config.lorioturl, (char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "expectedrssi")) {
          config.expectedrssi = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "rssibias")) {
          config.rssibias = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "expectedsnr")) {
          config.expectedsnr = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "snrbias")) {
          config.snrbias = atof((char *)event.data.scalar.value);
          fieldcounter++;
        } else if (!strcmp(name, "groups")) {
          fieldcounter++;
        } else if (!strcmp(name, "expectedlatency")) {
          fieldcounter++;
        } else if (strlen(name) == 0) {
          // ignore everything when in sequence
        } else {
          // this makes sure that if there are extra fields, load will fail
          printf("unknown config field \"%s\"\n", name);
          return 1;
        }
        value = false;
      } else {
        strcpy(name, (char *)event.data.scalar.value);
        strcpy(seqname, name);
        value = true;
      }
      break;
    }
    if (event.type != YAML_STREAM_END_EVENT)
      yaml_event_delete(&event);
  } while (event.type != YAML_STREAM_END_EVENT);
  yaml_event_delete(&event);
  yaml_parser_delete(&parser);
  fclose(fh);
  if (fieldcounter != numberoffields) {
    printf("number of fields %d does not match the ones being parsed %d\n",
           numberoffields, fieldcounter);
    return LGW_HAL_ERROR;
  }

  char *host = config.envhost;
  int port = config.envport;
  char portstr[10];
  sprintf(portstr, "%d", port);
  int clientfd = establishConnection(getHostInfo(host, portstr));
  if (clientfd == -1) {
    fprintf(stderr, "Failed to connect to: %s:%s \n", host, portstr);
    return LGW_HAL_ERROR;
  }
  printf("connection to environment estabished.\n");
  GatewayMetadata msg = GATEWAY_METADATA__INIT;
  char address[18];
  for (int i = 0; i < 17; i++) {
    address[i] = config.macaddress[i];
  }
  address[17] = 0;
  msg.macaddress = address;
  msg.latitude = config.latitude;
  msg.longitude = config.longitude;
  if (grouplen > 0) {
    msg.groups = malloc(grouplen * sizeof(*msg.groups));
    for (size_t i = 0; i < grouplen; i++) {
      printf("member of group id: %d\n", groups[i]);
      msg.groups[i] = (uint32_t)groups[i];
    }

    msg.n_groups = grouplen;
  } else {
    // should never happen
    msg.n_groups = 0;
    msg.groups = NULL;
  }
  int l = gateway_metadata__get_packed_size(&msg);
  uint8_t *buffer = malloc(l * sizeof(*buffer));
  gateway_metadata__pack(&msg, buffer);
  char *bufferhex = malloc(sizeof(*bufferhex) * 2 * l);
  char *buf2 = bufferhex;
  for (int i = 0; i < l; i++) {
    sprintf(&buf2[2 * i], "%02X", buffer[i]);
  }
  POST(clientfd, "/gateway/add", bufferhex, 2 * l);
  close(clientfd);
  free(buffer);
  if (msg.groups != NULL) {
    free(msg.groups);
  }
  conn_rec = amqp_new_connection();
  if (conn_rec == NULL) {
    fprintf(stderr, "Failed to create new amqp receive connection\n");
    return LGW_HAL_ERROR;
  }
  socket_rec = amqp_tcp_socket_new(conn_rec);
  if (socket_rec == NULL) {
    fprintf(stderr, "Failed to create new amqp connection\n");
    return LGW_HAL_ERROR;
  }
  int resp =
      amqp_socket_open(socket_rec, config.rabbitmqurl, config.rabbitmqport);
  if (resp != 0) {
    return LGW_HAL_ERROR;
  }

  amqp_rpc_reply_t resp_rpc = amqp_login(
      conn_rec, "/", AMQP_DEFAULT_MAX_CHANNELS, AMQP_DEFAULT_FRAME_SIZE, 0,
      AMQP_SASL_METHOD_PLAIN, config.rabbitmqusername, config.rabbitmqpassword);
  int r = check_amqp_error(resp_rpc, "login fail");
  if (r != 0) {
    return LGW_HAL_ERROR;
  }
  printf("socket_rec open and logged in\n");
  amqp_channel_open_ok_t *resp_channel_open = amqp_channel_open(conn_rec, 1);
  if (resp_channel_open == NULL) {
    printf("rec channel open failed\n");
    return LGW_HAL_ERROR;
  }
  resp = check_amqp_error(amqp_get_rpc_reply(conn_rec), "connection");
  if (resp != 0) {
    return LGW_HAL_ERROR;
  }

  conn_send = amqp_new_connection();
  if (conn_send == NULL) {
    fprintf(stderr, "Failed to create new amqp receive connection\n");
    return LGW_HAL_ERROR;
  }
  socket_send = amqp_tcp_socket_new(conn_send);
  if (socket_send == NULL) {
    fprintf(stderr, "Failed to create new amqp connection\n");
    return LGW_HAL_ERROR;
  }
  resp = amqp_socket_open(socket_send, config.rabbitmqurl, config.rabbitmqport);
  if (resp != 0) {
    return LGW_HAL_ERROR;
  }

  resp_rpc = amqp_login(conn_send, "/", AMQP_DEFAULT_MAX_CHANNELS,
                        AMQP_DEFAULT_FRAME_SIZE, 0, AMQP_SASL_METHOD_PLAIN,
                        config.rabbitmqusername, config.rabbitmqpassword);
  resp = check_amqp_error(resp_rpc, "login fail");
  if (resp != 0) {
    return LGW_HAL_ERROR;
  }
  printf("socket_send open and logged in\n");
  resp_channel_open = amqp_channel_open(conn_send, 1);
  if (resp_channel_open == NULL) {
    printf("send channel open failed\n");
    return LGW_HAL_ERROR;
  }
  resp = check_amqp_error(amqp_get_rpc_reply(conn_send), "connection");
  if (resp != 0) {
    return LGW_HAL_ERROR;
  }

  char qname[20];
  qname[0] = 'g';
  qname[1] = 'w';
  for (size_t i = 0; i < 17; i++) {
    qname[i + 2] = config.macaddress[i];
  }
  qname[19] = 0;
  printf("queue name: %s\n", qname);

  amqp_bytes_t qnamebytes = amqp_cstring_bytes(qname);

  amqp_queue_declare_ok_t *qresp =
      amqp_queue_declare(conn_rec, 1, qnamebytes, 0, 0, 0, 1, amqp_empty_table);
  if (qresp == NULL) {
    printf("declaring queue failed\n");
    return LGW_HAL_ERROR;
  }

  amqp_basic_consume_ok_t *cons = amqp_basic_consume(
      conn_rec, 1, qnamebytes, amqp_empty_bytes, 0, 0, 0, amqp_empty_table);
  if (cons == NULL) {
    printf("basic consuming failed\n");
    return LGW_HAL_ERROR;
  }

  pthread_mutex_init(&amqp_receive_lock, NULL);
  pthread_create(&amqp_receive_thread, NULL, (void *)amqp_receive, NULL);
  printf("lgw_start finished\n");
  started = true;
  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_stop(void) {
  char *host = config.envhost;
  int port = config.envport;
  char portstr[10];
  sprintf(portstr, "%d", port);
  int clientfd = establishConnection(getHostInfo(host, portstr));
  if (clientfd > 0) {
    GatewayMetadata msg = GATEWAY_METADATA__INIT;
    char address[18];
    for (int i = 0; i < 17; i++) {
      address[i] = config.macaddress[i];
    }
    address[17] = 0;
    msg.macaddress = address;
    msg.latitude = config.latitude;
    msg.longitude = config.longitude;
    int l = gateway_metadata__get_packed_size(&msg);
    uint8_t *buffer = malloc(l * sizeof(*buffer));
    gateway_metadata__pack(&msg, buffer);
    char *bufferhex = malloc(sizeof(*bufferhex) * 2 * l);
    char *buf2 = bufferhex;
    for (int i = 0; i < l; i++) {
      sprintf(&buf2[2 * i], "%02X", buffer[i]);
    }
    POST(clientfd, "/gateway/remove", bufferhex, 2 * l);
    close(clientfd);
    free(buffer);
  }
  pthread_mutex_destroy(&amqp_receive_lock);
  pthread_cancel(amqp_receive_thread);
  if (conn_rec != NULL) {
    amqp_connection_close(conn_rec, 0);
  }
  if (config.networkinterfacename != NULL) {
    free(config.networkinterfacename);
  }
  if (config.macaddress != NULL) {
    free(config.macaddress);
  }
  if (config.rabbitmqurl != NULL) {
    free(config.rabbitmqurl);
  }
  if (config.rabbitmqusername != NULL) {
    free(config.rabbitmqusername);
  }
  if (config.rabbitmqpassword != NULL) {
    free(config.rabbitmqpassword);
  }
  if (config.devupchannelname != NULL) {
    free(config.devupchannelname);
  }
  if (config.gwdnchannelname != NULL) {
    free(config.gwdnchannelname);
  }
  if (config.envhost != NULL) {
    free(config.envhost);
  }
  if (config.lorioturl != NULL) {
    free(config.lorioturl);
  }
  started = false;
  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_receive(uint8_t max_pkt, struct lgw_pkt_rx_s *pkt_data) {
  if (!started) {
    return LGW_HAL_ERROR;
  }
  int nb_packets = 0;
  pthread_mutex_lock(&amqp_receive_lock);
  for (size_t i = 0; i < max_pkt; i++) {
    if (uplinks[i] == NULL) {
      continue;
    }
    pkt_data[i] = *uplinks[i];
    free(uplinks[i]);
    uplinks[i] = NULL;
    nb_packets++;
  }
  // move up packets in the queue
  size_t moveto = 0;
  for (size_t i = 0; i < 16; i++) {
    if (uplinks[i] == NULL) {
      continue;
    }
    uplinks[moveto] = uplinks[i];
    uplinks[i] = NULL;
    moveto++;
  }
  pthread_mutex_unlock(&amqp_receive_lock);
  return nb_packets;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

static pthread_t dlthread;
static uint32_t dlsleep = 0;
static struct lgw_pkt_tx_s tx;
static volatile bool txscheduled = false;

void sendtx() {
  printf("sendtx\n");
  txscheduled = true;
  usleep(dlsleep);

  WithMetadataFromGateway message = WITH_METADATA_FROM_GATEWAY__INIT;
  message.bandwidth = tx.bandwidth;
  message.coderate = tx.coderate;
  message.frequency = tx.freq_hz;
  char address[18];
  for (int i = 0; i < 18; i++) {
    address[i] = config.macaddress[i];
  }
  message.macaddress = address;
  if (tx.modulation == MOD_LORA) {
    message.modulation = MODULATION__MOD_LORA;
  } else if (tx.modulation == MOD_FSK) {
    message.modulation = MODULATION__MOD_LORA;
  } else {
    message.modulation = MODULATION__MOD_UNDEFINED;
  }

  size_t length = 2 * tx.size + 1;
  char *bufferhex = malloc(sizeof(*bufferhex) * length);
  char *buf2 = bufferhex;
  for (int i = 0; i < tx.size; i++) {
    sprintf(&buf2[2 * i], "%02X", tx.payload[i]);
  }
  message.payload = (char *)bufferhex;
  bufferhex[length - 1] = 0;
  message.payloadsize = tx.size;
  message.spreadingfactor = tx.datarate;
  message.transmitsignalstrength = (double)tx.rf_power;
  size_t s = with_metadata_from_gateway__get_packed_size(&message);
  char *messagebody = malloc(sizeof(*messagebody) * s);
  with_metadata_from_gateway__pack(&message, messagebody);
  amqp_basic_properties_t props;
  props._flags = AMQP_BASIC_CONTENT_TYPE_FLAG;
  props.content_type = amqp_cstring_bytes("text/plain");

  amqp_bytes_t result;
  result.len = s;
  result.bytes = (void *)messagebody;

  int publish_response = amqp_basic_publish(
      conn_send, 1, amqp_empty_bytes,
      amqp_cstring_bytes(config.gwdnchannelname), 0, 0, &props, result);
  if (publish_response < 0) {
    fprintf(stderr, "%s: %s\n", "Publishing",
            amqp_error_string2(publish_response));
  }
  free(messagebody);
  free(bufferhex);
  dlsleep = 0;
  int timeonair = lgw_time_on_air(&tx);
  txscheduled = false;
  transmitting = true;
  usleep(timeonair * 1000);
  transmitting = false;
}

int lgw_send(struct lgw_pkt_tx_s pkt_data) {
  if (!started) {
    return LGW_HAL_ERROR;
  }
  if (txscheduled) {
    return LGW_HAL_ERROR;
  }
  if (transmitting) {
    return LGW_HAL_ERROR;
  }
  if (pkt_data.tx_mode == TIMESTAMPED) {
    uint32_t ts;
    lgw_get_trigcnt(&ts);
    dlsleep = pkt_data.count_us - ts;
    if (pkt_data.count_us < ts) {
      return LGW_HAL_ERROR;
    }
    tx = pkt_data;
    pthread_create(&dlthread, NULL, (void *)sendtx, NULL);
  } else if (pkt_data.tx_mode == IMMEDIATE) {
    dlsleep = 0;
    tx = pkt_data;
    pthread_create(&dlthread, NULL, (void *)sendtx, NULL);
  } else {
    return LGW_HAL_ERROR;
  }
  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_status(uint8_t select, uint8_t *code) {
  /* check input variables */
  CHECK_NULL(code);

  if (select == TX_STATUS) {
    if (started == false) {
      *code = TX_OFF;
    } else if (transmitting) {
      *code = TX_EMITTING;
    } else if (txscheduled) { /* bit 5 or 6 @1: TX sequence */
      *code = TX_SCHEDULED;
    } else {
      *code = TX_FREE;
    }
    return LGW_HAL_SUCCESS;

  } else if (select == RX_STATUS) {
    *code = RX_STATUS_UNKNOWN; /* todo */
    return LGW_HAL_SUCCESS;

  } else {
    DEBUG_MSG("ERROR: SELECTION INVALID, NO STATUS TO RETURN\n");
    return LGW_HAL_ERROR;
  }
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_abort_tx(void) {
  pthread_cancel(dlthread);
  txscheduled = false;
  transmitting = false;
  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int lgw_get_trigcnt(uint32_t *trig_cnt_us) {
  *trig_cnt_us = now_microseconds() - start;
  return LGW_HAL_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

const char *lgw_version_info() { return lgw_version_string; }

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

uint32_t lgw_time_on_air(struct lgw_pkt_tx_s *packet) {
  int32_t val;
  uint8_t SF, H, DE;
  uint16_t BW;
  uint32_t payloadSymbNb, Tpacket;
  double Tsym, Tpreamble, Tpayload, Tfsk;

  if (packet == NULL) {
    DEBUG_MSG("ERROR: Failed to compute time on air, wrong parameter\n");
    return 0;
  }

  if (packet->modulation == MOD_LORA) {
    /* Get bandwidth */
    val = lgw_bw_getval(packet->bandwidth);
    if (val != -1) {
      BW = (uint16_t)(val / 1E3);
    } else {
      DEBUG_PRINTF("ERROR: Cannot compute time on air for this packet, "
                   "unsupported bandwidth (0x%02X)\n",
                   packet->bandwidth);
      return 0;
    }

    /* Get datarate */
    val = lgw_sf_getval(packet->datarate);
    if (val != -1) {
      SF = (uint8_t)val;
    } else {
      DEBUG_PRINTF("ERROR: Cannot compute time on air for this packet, "
                   "unsupported datarate (0x%02X)\n",
                   packet->datarate);
      return 0;
    }

    /* Duration of 1 symbol */
    Tsym = pow(2, SF) / BW;

    /* Duration of preamble */
    Tpreamble = ((double)(packet->preamble) + 4.25) * Tsym;

    /* Duration of payload */
    H = (packet->no_header == false)
            ? 0
            : 1; /* header is always enabled, except for beacons */
    DE = (SF >= 11)
             ? 1
             : 0; /* Low datarate optimization enabled for SF11 and SF12 */

    payloadSymbNb =
        8 + (ceil((double)(8 * packet->size - 4 * SF + 28 + 16 - 20 * H) /
                  (double)(4 * (SF - 2 * DE))) *
             (packet->coderate + 4)); /* Explicitely cast to double to keep
                                         precision of the division */

    Tpayload = payloadSymbNb * Tsym;

    /* Duration of packet */
    Tpacket = Tpreamble + Tpayload;
  } else if (packet->modulation == MOD_FSK) {
    /* PREAMBLE + SYNC_WORD + PKT_LEN + PKT_PAYLOAD + CRC
            PREAMBLE: default 5 bytes
            SYNC_WORD: default 3 bytes
            PKT_LEN: 1 byte (variable length mode)
            PKT_PAYLOAD: x bytes
            CRC: 0 or 2 bytes
    */
    Tfsk = (8 *
            (double)(packet->preamble + fsk_sync_word_size + 1 + packet->size +
                     ((packet->no_crc == true) ? 0 : 2)) /
            (double)packet->datarate) *
           1E3;

    /* Duration of packet */
    Tpacket = (uint32_t)Tfsk + 1; /* add margin for rounding */
  } else {
    Tpacket = 0;
    DEBUG_PRINTF("ERROR: Cannot compute time on air for this packet, "
                 "unsupported modulation (0x%02X)\n",
                 packet->modulation);
  }

  return Tpacket;
}

/* --- EOF ------------------------------------------------------------------ */
