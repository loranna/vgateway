FROM registry.gitlab.com/loriot/agent/base:latest

WORKDIR /gateway
COPY ./ /gateway
RUN make
ENTRYPOINT [ "/bin/bash" ]