# CHANGELOG

## 0.2.47

- added gps support
- fixed a bug with downlink
- now vgateway respects the radio values coming from the enviroment

## 0.1.33

- reads rabbitmq queue for uplinks
- writes reabbitmq queue with downlinks
